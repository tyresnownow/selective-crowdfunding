var oneDollar = 365;
var oneSlot = 25;
var apiUrl = "https://api.timiakogun.com/en/services/";
var totalSlots = 600;
var slotAvailable = 600;
var amountInNaira = 0;



function numberWithCommas(x) {
    x = x.toString();
    var pattern = /(-?\d+)(\d{3})/;
    while (pattern.test(x))
        x = x.replace(pattern, "$1,$2");
    return x;
}

$('.available').html(slotAvailable);
var parts = window.location.search.substr(1).split("&");
var getvars = {};
for (var i = 0; i < parts.length; i++) {
    var temp = parts[i].split("=");
    getvars[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
}
$(".greetings .name").html((getvars['to'] != null) ? " " + getvars['to'] : '');
$("#first_name").val((getvars['to'] != null) ? " "+getvars['to']:'');

$("#slot").keyup(function() {
   valitdateSlots()
});


$("#slot").change(function () {
    valitdateSlots()
});

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function validatePhone(phone_number){
    var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
    return re.test(String(phone_number).toLowerCase());
}

$('#email').keyup(function() {
    $('label[for="email"] .caution').html(validateEmail($(this).val()) ? '' : "Email is not looking perfect yet");
})

$('#phone_number').keyup(function () {
    $('label[for="phone_number"] .caution').html(validatePhone($(this).val()) ? '' : "Phone Number is not looking perfect yet");
})

function valitdateSlots() {
    var slots = $('#slot').val();
    totalUSD = slots * oneSlot;
    $(".amount").html('$' + numberWithCommas(totalUSD));

    amountInNaira = totalUSD * oneDollar;
    var summary = '';
    if (totalUSD > 0) {
        summary += 'Number of slots = ' + slots;
        summary += '<br>';
        summary += 'Number of value in USD = ' + numberWithCommas(totalUSD);
        summary += '<br>';
        summary += 'Convertion to Naira* = <strong> &#8358;' + numberWithCommas(amountInNaira) + '</strong>'
        summary += '<br>';
        summary += '<small>*Naira conversion is at the rate of &#8358;' + oneDollar + '/$</small>';
        $('label[for="slot"] .caution').html((slots <= slotAvailable) ? '' : "You have entered more than available slots");
    } else {
        $('label[for="slot"] .caution').html("Must be between 1 and " + slotAvailable + " Slots");
    }

    $('.purchase-summary').html(summary);
}

$.ajax
    ({
        type: "POST",
        url: apiUrl + "slots_available",
        dataType: 'json',
        success: function (data) {
            available = data.available; 
            slotAvailable = totalSlots - available;
            $('.available').html(slotAvailable);
        }
});

function isValEmpty(id) {
    
    console.log(id + ' ' + $('#' + id).val() == '');
    
    return $('#'+id).val() == '';
}

function isFilledForm() {
    return (!isValEmpty('first_name') && !isValEmpty('last_name') && !isValEmpty('phone_number') && !isValEmpty('email') && !isValEmpty('slot'));
}

function isvalidForm() {
    var isValid = true;
    $('.caution').each(function () {
        if ( $(this).html() != '') isValid = false;
    })
    return isValid;
}

$('.purchase').click(function () {

    if (!isFilledForm()) {
        $('.error').html('Fill all fields to pay');
        return false;
    }

    if (!isvalidForm()) {
        $('.error').html('Fill all fields correctly to pay');
        return false;
    }
    $('body').css({ 'opacity': '0.2' });
    var first_name = $("#first_name").val();
    var last_name = $("#last_name").val();
    var email = $("#email").val();
    var phone_number = $("#phone_number").val();
    var slots = $("#slot").val();

    $.ajax
        ({
            type: "POST",
            url: apiUrl + 'slot',
            dataType: 'json',
            data: {
                'first_name': first_name,
                'last_name': last_name,
                'email': email,
                'phone_number': phone_number,
                'slots': slots
            },
            success: function (data) {
              
                $('body').css({ 'opacity': '1' });

                var handler = PaystackPop.setup({
                    key: data.paystack_public_key,
                    email: email,
                    amount: amountInNaira*100,
                    ref: data.id,
                    currency: 'NGN',
                    metadata: {
                        custom_fields: [
                          {
                            display_name: "Slot purchase",
                            variable_name: first_name+' '+last_name,
                            value: phone_number
                          }
                        ]
                    },
                    callback: function (response) {
                        confirmPayment(data.id)
                    },
                    onClose: function () {
                        // alert('window closed');
                    }
                });
                handler.openIframe();
            }
        });


    $('body').css({'opacity': '0.2'});

});
$('input').keyup(function () {
    $('.error').html('');
})

function confirmPayment(id) {
    $.ajax
        ({
            type: "GET",
            url: apiUrl + 'paystack_confirm?id='+id,
            dataType: 'json',
            success: function (data) {
                $('#alert .msg').html(data.verbose);
                $('#alert').removeClass('hidden');
                if (data.error == 0) {
                    $('#alert').addClass('alert-success');
                    $('#alert').removeClass('alert-warning');
                    $('#alert .msg').append("<strong>You deserve more than an automated \"thank you\" mail. I will rather call you or send a personalized email for you soon <3</strong>");

                }else {
                    $('#alert').addClass('alert-warning');
                    $('#alert').removeClass('alert-success');
                }
                $('input').val("");

            }
        });
    
}